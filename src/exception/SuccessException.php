<?php
declare(strict_types=1);

namespace think\admin\exception;

class SuccessException extends BaseException
{
    /**
     * @var int
     */
    public int $statusCode = 200;

    /**
     * @var string
     */
    public string $errorMessage = 'Validation Success';
}
