<?php
declare (strict_types=1);

namespace think\admin\service;

use think\admin\Library;
use think\admin\Service;

/**
 * 系统模块管理
 * @class ModuleService
 * @package think\admin\service
 */
class ModuleService extends Service
{
    /**
     * 获取 PHP 执行路径
     * @return string
     */
    public static function getPhpExec(): string
    {
        if ($phpExec = sysvar('phpBinary')) return $phpExec;
        $phpExec = str_replace('/sbin/php-fpm', '/bin/php', PHP_BINARY);
        $phpExec = preg_replace('#-(cgi|fpm)(\.exe)?$#', '$2', $phpExec);
        return sysvar('phpBinary', ProcessService::isFile($phpExec) ? $phpExec : 'php');
    }

    /**
     * 获取应用模块
     * @param array $data
     * @return array
     */
    public static function getModules(array $data = []): array
    {
        $path = app_path();
        foreach (scandir($path) as $item) if ($item[0] !== '.') {
            if (is_dir(realpath($path . $item))) $data[] = $item;
        }
        return $data;
    }
}