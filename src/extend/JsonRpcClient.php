<?php
declare (strict_types=1);

namespace think\admin\extend;

use support\exception\BusinessException;

/**
 * JsonRpc 客户端
 * @class JsonRpcClient
 * @package think\admin\extend
 */
class JsonRpcClient
{
    /**
     * 请求ID
     * @var integer
     */
    private $id;

    /**
     * 服务端地址
     * @var string
     */
    private $proxy;

    /**
     * 请求头部参数
     * @var string
     */
    private $header;

    /**
     * JsonRpcClient constructor.
     * @param string $proxy
     * @param array $header
     */
    public function __construct(string $proxy, array $header = [])
    {
        $this->id = time();
        $this->proxy = $proxy;
        $this->header = $header;
    }

    /**
     * 执行 JsonRpc 请求
     * @param string $method
     * @param array $params
     * @return mixed
     */
    public function __call(string $method, array $params = [])
    {
        $options = [
            'ssl'  => [
                'verify_peer'      => false,
                'verify_peer_name' => false,
            ],
            'http' => [
                'method'  => 'POST', "timeout" => 60,
                'header'  => join("\r\n", array_merge(['Content-Type:application/json'], $this->header, ['User-Agent:think-admin-jsonrpc', ''])),
                'content' => json_encode(['jsonrpc' => '2.0', 'method' => $method, 'params' => $params, 'id' => $this->id], JSON_UNESCAPED_UNICODE),
            ],
        ];
        try {
            // Performs the HTTP POST
            if ($fp = fopen($this->proxy, 'r', false, stream_context_create($options))) {
                $response = '';
                while ($line = fgets($fp)) $response .= trim($line) . "\n";
                [, $response] = [fclose($fp), json_decode($response, true)];
            } else {
                throw new BusinessException(lang("Unable connect: %s", [$this->proxy]));
            }
        } catch (BusinessException $exception) {
            throw $exception;
        } catch (\Exception $exception) {
            return new BusinessException($exception->getMessage());
        }
        // Compatible with normal
        if (isset($response['code']) && isset($response['info'])) {
            return new BusinessException($response['info'], intval($response['code']), $response['data'] ?? []);
        }
        // Final checks and return
        if (empty($response['id']) || $response['id'] != $this->id) {
            return new BusinessException(lang("Error flag ( Request tag: %s, Response tag: %s )", [$this->id, $response['id'] ?? '-']), 0, $response);
        }
        if (is_null($response['error'])) return $response['result'];
        return new BusinessException($response['error']['message'], intval($response['error']['code']), $response['result'] ?? []);
    }
}